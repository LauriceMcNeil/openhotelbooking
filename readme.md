## Open Hotel Booking

With the recent updates and consistent changes in the field of business, every company have been doing their best to excel from their competitors. And the accommodation industry is just one of them. Many resorts and hotels are now going online, and booking for your trip can now be down with your laptop or even mobiles.

Below is an example of how an online booking flows. Check out [here](https://www.psychz.net/dedicated-hosting.html) is you want to know more about this process. 

### Setting up
#### Run migrations
```
php artisan migrate
```

#### Seed the database
```
php artisan db:seed
```

#### Configuration
Edit configuration files in `app/config` directory.

### Admin Login
Go to `/admin/login` and enter the following credentials:<br>
Email: admin@example.com<br>
Password: 12345678<br>